#pragma once

namespace fast_io::curve25519
{

inline constexpr void addition_u256_discard(std::uint_least64_t* __restrict z,
	std::uint_least64_t const* __restrict x,
	std::uint_least64_t const* __restrict y) noexcept
{
	using namespace ::fast_io::details::intrinsics;
	bool carry{add_carry(false,x[0],y[0],z[0])};
	carry=add_carry(carry,x[1],y[1],z[1]);
	carry=add_carry(carry,x[2],y[2],z[2]);
	add_carry(carry,x[3],y[3],z[3]);
}

inline constexpr std::uint_least64_t addition_u256(std::uint_least64_t* __restrict z,
	std::uint_least64_t const* __restrict x,
	std::uint_least64_t const* __restrict y) noexcept
{
	using namespace ::fast_io::details::intrinsics;
	using unsigned_type = field_number::value_type;
	bool carry{add_carry(false,x[0],y[0],z[0])};
	carry=add_carry(carry,x[1],y[1],z[1]);
	carry=add_carry(carry,x[2],y[2],z[2]);
	carry=add_carry(carry,x[3],y[3],z[3]);
	constexpr unsigned_type zero{};
	unsigned_type res;
	add_carry(carry,zero,zero,res);
	return res;
}

inline constexpr void field_number_addition(std::uint_least64_t* f,
	std::uint_least64_t const* x,
	std::uint_least64_t const* y) noexcept
{
	using namespace ::fast_io::details::intrinsics;
	using unsigned_type = field_number::value_type;
	constexpr unsigned_type zero{};
	std::uint_least64_t f0,f1,f2,f3;
	bool carry{add_carry(false,x[0],y[0],f0)};
	carry=add_carry(carry,x[1],y[1],f1);
	carry=add_carry(carry,x[2],y[2],f2);
	carry=add_carry(carry,x[3],y[3],f3);
	unsigned_type v{};
	sub_borrow(carry,v,v,v);
	v&=static_cast<unsigned_type>(38);
	carry=add_carry(false,f0,v,f0);
	carry=add_carry(carry,f1,zero,f1);
	carry=add_carry(carry,f2,zero,f2);
	carry=add_carry(carry,f3,zero,f3);
	sub_borrow(carry,v,v,v);
	v&=static_cast<unsigned_type>(38);
	carry=add_carry(false,f0,v,f[0]);
	carry=add_carry(carry,f1,zero,f[1]);
	carry=add_carry(carry,f2,zero,f[2]);
	carry=add_carry(carry,f3,zero,f[3]);
}

inline constexpr void field_number_subtraction(std::uint_least64_t* f,
	std::uint_least64_t const* x,
	std::uint_least64_t const* y) noexcept
{
	using namespace ::fast_io::details::intrinsics;
	using unsigned_type = field_number::value_type;
	constexpr unsigned_type zero{};
	std::uint_least64_t f0,f1,f2,f3;
	bool borrow{sub_borrow(false,x[0],y[0],f0)};
	borrow=sub_borrow(borrow,x[1],y[1],f1);
	borrow=sub_borrow(borrow,x[2],y[2],f2);
	borrow=sub_borrow(borrow,x[3],y[3],f3);
	unsigned_type v{};
	sub_borrow(borrow,v,v,v);
	v&=static_cast<unsigned_type>(38);
	borrow=sub_borrow(false,f0,v,f0);
	borrow=sub_borrow(borrow,f1,zero,f1);
	borrow=sub_borrow(borrow,f2,zero,f2);
	borrow=sub_borrow(borrow,f3,zero,f3);
	sub_borrow(borrow,v,v,v);
	v&=static_cast<unsigned_type>(38);
	borrow=sub_borrow(false,f0,v,f[0]);
	borrow=sub_borrow(borrow,f1,zero,f[1]);
	borrow=sub_borrow(borrow,f2,zero,f[2]);
	borrow=sub_borrow(borrow,f3,zero,f[3]);
}

template<bool first=false>
inline constexpr void multiply_single(std::uint_least64_t* __restrict dest,std::uint_least64_t const* __restrict x,std::uint_least64_t b) noexcept
{
	using namespace ::fast_io::details::intrinsics;
	std::uint_least64_t h0;
	std::uint_least64_t t0{umul(x[0],b,h0)};
	std::uint_least64_t h1;
	std::uint_least64_t t1{umul(x[1],b,h1)};
	std::uint_least64_t h2;
	std::uint_least64_t t2{umul(x[2],b,h2)};
	std::uint_least64_t h3;
	std::uint_least64_t t3{umul(x[3],b,h3)};
	constexpr std::uint_least64_t zero{};
	if constexpr(first)
	{
		dest[0]=t0;
		bool carry{add_carry(false,t1,h0,dest[1])};
		carry=add_carry(carry,t2,h1,dest[2]);
		carry=add_carry(carry,t3,h2,dest[3]);
		add_carry(carry,zero,h3,dest[4]);
	}
	else
	{
		bool carry{add_carry(false,t1,h0,t1)};
		carry=add_carry(carry,t2,h1,t2);
		carry=add_carry(carry,t3,h2,t3);
		add_carry(carry,zero,h3,h3);

		carry=add_carry(false,t0,dest[0],dest[0]);
		carry=add_carry(carry,t1,dest[1],dest[1]);
		carry=add_carry(carry,t2,dest[2],dest[2]);
		carry=add_carry(carry,t3,dest[3],dest[3]);
		add_carry(carry,zero,h3,dest[4]);
	}
}

inline constexpr void multiplication_add_reduce(std::uint_least64_t* z, std::uint_least64_t const* y, std::uint_least64_t constant, std::uint_least64_t const* x) noexcept
{
	using namespace ::fast_io::details::intrinsics;
	constexpr std::uint_least64_t zero{};
	std::uint_least64_t h0;
	std::uint_least64_t t0{umul(*x,constant,h0)};
	std::uint_least64_t h1;
	std::uint_least64_t t1{umul(x[1],constant,h1)};
	std::uint_least64_t h2;
	std::uint_least64_t t2{umul(x[2],constant,h2)};
	std::uint_least64_t h3;
	std::uint_least64_t t3{umul(x[3],constant,h3)};

	bool carry{add_carry(false,t0,y[0],t0)};
	carry=add_carry(carry,t1,y[1],t1);
	carry=add_carry(carry,t2,y[2],t2);
	carry=add_carry(carry,t3,y[3],t3);
	add_carry(carry,h3,zero,h3);


	carry=add_carry(zero,t1,h0,t1);
	carry=add_carry(carry,t2,h1,t2);
	carry=add_carry(carry,t3,h2,t3);
	add_carry(carry,h3,zero,h3);

	constexpr std::uint_least64_t constant38{38};

	std::uint_least64_t v;
	std::uint_least64_t t4{umul(h3,constant38,v)};
	carry=add_carry(false,t0,t4,t0);
	carry=add_carry(carry,t1,v,t1);
	carry=add_carry(carry,t2,zero,t2);
	carry=add_carry(carry,t3,zero,t3);
	add_carry(carry,zero,zero,h3);


	sub_borrow(carry,v,v,t4);
	t4&=constant38;
	carry=add_carry(false,t0,t4,z[0]);
	carry=add_carry(carry,t1,zero,z[1]);
	carry=add_carry(carry,t2,zero,z[2]);
	add_carry(carry,t3,zero,z[3]);
}

inline constexpr void reduce_final(std::uint_least64_t* __restrict f, std::uint_least64_t const* x) noexcept
{
	constexpr std::uint_least64_t constant{38};
	multiplication_add_reduce(f,x,constant,x+4);
}


inline constexpr void field_number_multiplication(std::uint_least64_t* r,std::uint_least64_t const* x,std::uint_least64_t const* y) noexcept
{
	std::uint_least64_t t[8];
	multiply_single<true>(t,x,y[0]);
	multiply_single(t+1,x,y[1]);
	multiply_single(t+2,x,y[2]);
	multiply_single(t+3,x,y[3]);
	reduce_final(r,t);
}

inline constexpr std::uint_least64_t muladd_w0(std::uint_least64_t add_value,std::uint_least64_t mul_value,std::uint_least64_t& high) noexcept
{
	using namespace ::fast_io::details::intrinsics;
	constexpr std::uint_least64_t constant{38};
	constexpr std::uint_least64_t zero{};
	std::uint_least64_t low{umul(mul_value,constant,high)};
	bool carry{add_carry(false,add_value,low,low)};
	add_carry(carry,zero,high,high);
	return low;
}

inline constexpr std::uint_least64_t muladd_w1(std::uint_least64_t add_value,std::uint_least64_t mul_value,std::uint_least64_t& high,std::uint_least64_t last_high) noexcept
{
	using namespace ::fast_io::details::intrinsics;
	constexpr std::uint_least64_t constant{38};
	constexpr std::uint_least64_t zero{};
	std::uint_least64_t low{umul(mul_value,constant,high)};
	add_carry(add_carry(false,last_high,low,low),zero,high,high);
	add_carry(add_carry(false,add_value,low,low),zero,high,high);
	return low;
}

inline constexpr void field_number_square(std::uint_least64_t* r,std::uint_least64_t const* x) noexcept
{
	using namespace ::fast_io::details::intrinsics;
	constexpr std::uint_least64_t zero{};
	constexpr std::uint_least64_t constant{38};

	std::uint_least64_t x0{*x},x1{x[1]},x2{x[2]},x3{x[3]};

	std::uint_least64_t a2;
	std::uint_least64_t a1{umul(x0,x1,a2)};

	std::uint_least64_t b0;
	std::uint_least64_t a3{umul(x0,x3,b0)};

	std::uint_least64_t b2;
	std::uint_least64_t b1{umul(x2,x3,b2)};

	std::uint_least64_t b3_x0x2;
	std::uint_least64_t a0{umul(x0,x2,b3_x0x2)};

	std::uint_least64_t c1;
	std::uint_least64_t c0{umul(x1,x3,c1)};

	std::uint_least64_t c0_x1x2;
	std::uint_least64_t b3{umul(x1,x2,c0_x1x2)};

	bool carry{add_carry(false,b3_x0x2,b3,b3)};
	carry=add_carry(carry,c0,c0_x1x2,c0);
	add_carry(carry,zero,c1,c1);

	carry=add_carry(false,a0,a2,a2);
	carry=add_carry(carry,b3,a3,a3);
	carry=add_carry(carry,c0,b0,b0);
	carry=add_carry(carry,c1,b1,b1);
	add_carry(carry,zero,b2,b2);

// Multiply by 2
	carry=add_carry(false,a1,a1,a1);
	carry=add_carry(carry,a2,a2,a2);
	carry=add_carry(carry,a3,a3,a3);
	carry=add_carry(carry,b0,b0,b0);
	carry=add_carry(carry,b1,b1,b1);
	carry=add_carry(carry,b2,b2,b2);
	add_carry(carry,zero,zero,b3);

	std::uint_least64_t y1;
	x0=umul(x0,x0,y1);

	std::uint_least64_t y3;
	x1=umul(x1,x1,y3);

	std::uint_least64_t y5;
	x2=umul(x2,x2,y5);

	std::uint_least64_t y7;
	x3=umul(x3,x3,y7);

	carry=add_carry(false,a1,y1,y1);
	carry=add_carry(carry,a2,x1,x1);
	carry=add_carry(carry,a3,y3,y3);
	carry=add_carry(carry,b0,x2,x2);
	carry=add_carry(carry,b1,y5,y5);
	carry=add_carry(carry,b2,x3,x3);
	carry=add_carry(carry,b3,y7,y7);

	std::uint_least64_t high;
	a0=muladd_w0(x0,x2,high);
	a1=muladd_w1(y1,y5,high,high);
	a2=muladd_w1(x1,x3,high,high);
	a3=muladd_w1(y3,y7,high,high);

	std::uint_least64_t low{umul(constant,high,high)};

	carry=add_carry(false,low,a0,a0);
	carry=add_carry(carry,high,a1,a1);
	carry=add_carry(carry,zero,a2,a2);
	carry=add_carry(carry,zero,a3,a3);
	sub_borrow(carry,low,low,low);

	low&=constant;
	carry=add_carry(false,low,a0,*r);
	carry=add_carry(carry,zero,a1,r[1]);
	carry=add_carry(carry,zero,a2,r[2]);
	add_carry(carry,zero,a3,r[3]);
}

/*
Reference from mehdi
The curve used is y2 = x^3 + 486662x^2 + x, a Montgomery curve, over 
the prime field defined by the prime number 2^255 - 19, and it uses the 
base point x = 9.

Protocol uses compressed elliptic point (only X coordinates), so it 
allows for efficient use of the Montgomery ladder for ECDH, using only 
XZ coordinates.

The curve is birationally equivalent to Ed25519 (Twisted Edwards curve).

b = 256
p = 2**255 - 19
l = 2**252 + 27742317777372353535851937790883648493
*/

/*
Using Euler theorem to calculate the z^(-2) of an element over a finite field
2^255 - 21
*/

inline constexpr void field_number_pow_minus2(std::uint_least64_t* __restrict r,std::uint_least64_t const* z) noexcept
{
	using array_type = std::uint_least64_t[4];
	array_type t0,t1,z2,z9,z11;
	array_type z2_5_0,z2_10_0,z2_20_0,z2_50_0,z2_100_0;

/*
Montgomery ladder
*/

  /* 2 */               field_number_square(z2,z);
  /* 4 */               field_number_square(t1,z2);
  /* 8 */               field_number_square(t0,t1);
  /* 9 */               field_number_multiplication(z9,t0,z);
  /* 11 */              field_number_multiplication(z11,z9,z2);
  /* 22 */              field_number_square(t0,z11);
  /* 2^5 - 2^0 = 31 */  field_number_multiplication(z2_5_0,t0,z9);

  /* 2^6 - 2^1 */       field_number_square(t0,z2_5_0);
  /* 2^7 - 2^2 */       field_number_square(t1,t0);
  /* 2^8 - 2^3 */       field_number_square(t0,t1);
  /* 2^9 - 2^4 */       field_number_square(t1,t0);
  /* 2^10 - 2^5 */      field_number_square(t0,t1);
  /* 2^10 - 2^0 */      field_number_multiplication(z2_10_0,t0,z2_5_0);

  /* 2^11 - 2^1 */      field_number_square(t0,z2_10_0);
  /* 2^12 - 2^2 */      field_number_square(t1,t0);
  /* 2^20 - 2^10 */     for(std::uint_fast8_t i{};i!=4;++i) { 
                            field_number_square(t0,t1); 
                            field_number_square(t1,t0); }
  /* 2^20 - 2^0 */      field_number_multiplication(z2_20_0,t1,z2_10_0);

  /* 2^21 - 2^1 */      field_number_square(t0,z2_20_0);
  /* 2^22 - 2^2 */      field_number_square(t1,t0);

  /* 2^40 - 2^20 */     for(std::uint_fast8_t i{};i!=9;++i)
  			{ 
				field_number_square(t0,t1); 
  				field_number_square(t1,t0);
			}
  /* 2^40 - 2^0 */      field_number_multiplication(t0,t1,z2_20_0);

  /* 2^41 - 2^1 */      field_number_square(t1,t0);
  /* 2^42 - 2^2 */      field_number_square(t0,t1);
  /* 2^50 - 2^10 */     for(std::uint_fast8_t i{};i!=4;++i) { 
                            field_number_square(t1,t0); 
                            field_number_square(t0,t1); }
  /* 2^50 - 2^0 */      field_number_multiplication(z2_50_0,t0,z2_10_0);

  /* 2^51 - 2^1 */      field_number_square(t0,z2_50_0);
  /* 2^52 - 2^2 */      field_number_square(t1,t0);
  /* 2^100 - 2^50 */    for(std::uint_fast8_t i{};i!=24;++i) { 
                            field_number_square(t0,t1); 
                            field_number_square(t1,t0); }
  /* 2^100 - 2^0 */     field_number_multiplication(z2_100_0,t1,z2_50_0);

  /* 2^101 - 2^1 */     field_number_square(t1,z2_100_0);
  /* 2^102 - 2^2 */     field_number_square(t0,t1);
  /* 2^200 - 2^100 */   for(std::uint_fast8_t i{};i!=49;++i) { 
                            field_number_square(t1,t0); 
                            field_number_square(t0,t1); }
  /* 2^200 - 2^0 */     field_number_multiplication(t1,t0,z2_100_0);

  /* 2^201 - 2^1 */     field_number_square(t0,t1);
  /* 2^202 - 2^2 */     field_number_square(t1,t0);
  /* 2^250 - 2^50 */    for(std::uint_fast8_t i{};i!=24;++i) { 
                            field_number_square(t0,t1); 
                            field_number_square(t1,t0); }
  /* 2^250 - 2^0 */     field_number_multiplication(t0,t1,z2_50_0);

  /* 2^251 - 2^1 */     field_number_square(t1,t0);
  /* 2^252 - 2^2 */     field_number_square(t0,t1);
  /* 2^253 - 2^3 */     field_number_square(t1,t0);
  /* 2^254 - 2^4 */     field_number_square(t0,t1);
  /* 2^255 - 2^5 */     field_number_square(t1,t0);
  /* 2^255 - 21 */      field_number_multiplication(r,t1,z11);
}

/*
Using Euler theorem to calculate the inverse of an element over a finite field.
2^255 - 20
*/


inline constexpr field_number pow_minus2(field_number const& z) noexcept
{
	field_number number;
	field_number_pow_minus2(number.content,z.content);
	return number;
}

inline constexpr void field_number_reduce_to_25519(std::uint_least64_t* x) noexcept
{
	using namespace ::fast_io::details::intrinsics;
	constexpr std::uint_least64_t c1{UINT64_MAX};
	constexpr std::uint_least64_t c2_constant{c1-18};
	constexpr std::uint_least64_t c1r1_constant{c1>>1};
	std::uint_least64_t c2{c2_constant};
	std::uint_least64_t c1r1{c1r1_constant};
	std::uint_least64_t t{};
	std::uint_least64_t r0,r1,r2,r3;
	bool carry{sub_borrow(false,x[0],c2,r0)};
	carry=sub_borrow(carry,x[1],c1,r1);
	carry=sub_borrow(carry,x[2],c1,r2);
	carry=sub_borrow(carry,x[3],c1r1,r3);
	sub_borrow(carry,t,t,t);
//undo if carry is true
	c1r1&=t;
	c2&=t;

	carry=add_carry(false,r0,c2,r0);
	carry=add_carry(carry,r1,t,r1);
	carry=add_carry(carry,r2,t,r2);
	add_carry(carry,r3,c1r1,r3);

	c1r1=c1r1_constant;
	c2=c2_constant;

// we need to do this 2nd time.
	carry=sub_borrow(false,r0,c2,r0);
	carry=sub_borrow(carry,r1,c1,r1);
	carry=sub_borrow(carry,r2,c1,r2);
	carry=sub_borrow(carry,r3,c1r1,r3);
//undo if carry is true
	sub_borrow(carry,t,t,t);
	c1r1&=t;
	c2&=t;
	carry=add_carry(false,r0,c2,x[0]);
	carry=add_carry(carry,r1,t,x[1]);
	carry=add_carry(carry,r2,t,x[2]);
	add_carry(carry,r3,c1r1,x[3]);
}

inline constexpr void field_number_inverse(std::uint_least64_t* r,std::uint_least64_t const* z) noexcept
{
	field_number_pow_minus2(r,z);
	field_number_reduce_to_25519(r);
}

inline constexpr void field_multiplication_mod(std::uint_least64_t* z,std::uint_least64_t const* x,std::uint_least64_t const* y) noexcept
{
	field_number_multiplication(z,x,y);
	field_number_reduce_to_25519(z);
}

inline constexpr field_number inverse(field_number const& z) noexcept
{
	field_number number;
	field_number_inverse(number.content,z.content);
	return number;
}

inline constexpr field_number operator*(field_number const& x,field_number const& y) noexcept
{
	field_number f;
	field_multiplication_mod(f.content,x.content,y.content);
	return f;
}

struct xz_point
{
	field_number x,z;
};

inline constexpr std::byte montgomery_curve_base_point_x[32]{std::byte{9}};

/* Y = X + X */
inline constexpr void montgomery_curve_point_double(xz_point& y,xz_point const& x) noexcept
{
	field_number a,b;
/*  x2 = (x+z)^2 * (x-z)^2 */
/*  z2 = ((x+z)^2 - (x-z)^2)*((x+z)^2 + ((A-2)/4)((x+z)^2 - (x-z)^2)) */
	field_number_addition(a.content,x.x.content,x.z.content);
	field_number_subtraction(b.content,x.x.content,x.z.content);
	field_number_square(a.content,a.content);
	field_number_square(b.content,b.content);
	field_number_multiplication(y.x.content,a.content,b.content);
	field_number_subtraction(b.content,a.content,b.content);

	constexpr std::uint_least64_t constant{121665};
	multiplication_add_reduce(a.content,a.content,constant,b.content);
	field_number_multiplication(y.z.content,a.content,b.content);
}

inline constexpr void montgomery_curve_mont(xz_point& p,xz_point& q,field_number const& base) noexcept
{
	field_number a,b,c,d,e;
	field_number_subtraction(a.content,p.x.content,p.z.content);
	field_number_addition(b.content,p.x.content,p.z.content);
	field_number_subtraction(c.content,q.x.content,q.z.content);
	field_number_addition(d.content,q.x.content,q.z.content);
	field_number_multiplication(a.content,a.content,d.content);
	field_number_multiplication(b.content,b.content,c.content);
	field_number_addition(e.content,a.content,b.content);
	field_number_subtraction(b.content,a.content,b.content);
	field_number_square(p.x.content,e.content);
	field_number_square(a.content,b.content);
	field_number_multiplication(p.z.content,a.content,base.content);

	field_number_square(a.content,d.content);
	field_number_square(b.content,c.content);

	field_number_multiplication(q.x.content,a.content,b.content);
	field_number_subtraction(b.content,a.content,b.content);

	constexpr std::uint_least64_t constant{121665};
	multiplication_add_reduce(a.content,a.content,constant,b.content);
	field_number_multiplication(q.z.content,a.content,b.content);
}

inline
#if __cpp_lib_bit_cast >= 201806L && __cpp_lib_is_constant_evaluated >= 201811L
constexpr
#endif
void montgomery_curve_point_multiplication(std::byte* public_key,std::byte const* base_point,std::byte const* secret_key) noexcept
{
	constexpr std::size_t total_len{32};
	field_number x;
#if __cpp_lib_bit_cast >= 201806L && __cpp_lib_is_constant_evaluated >= 201811L
	if(__builtin_is_constant_evaluated())
	{
		::fast_io::freestanding::array<std::byte,total_len> buffer;
		::fast_io::freestanding::nonoverlapped_bytes_copy_n(base_point,total_len,buffer.data());
		x=__builtin_bit_cast(field_number,buffer);
	}
	else
#endif
	{
		::fast_io::freestanding::nonoverlapped_bytes_copy_n(base_point,total_len,reinterpret_cast<std::byte*>(x.content));
	}
	xz_point p,q;
	xz_point *pp[2];
	xz_point *qp[2];
	for(std::size_t j{total_len};j--;)
	{
		char unsigned k{static_cast<char unsigned>(secret_key[j])};
		for(std::size_t i{};i<8;++i)
		{
			if(k&0x80)
			{
				addition_u256_discard(p.z.content,x.content,::fast_io::curve25519::custom_blindings::zr.content);
				field_number_multiplication(p.x.content,x.content,p.z.content);
				montgomery_curve_point_double(q,p);
				*pp=__builtin_addressof(q);
				pp[1]=__builtin_addressof(p);
				*qp=__builtin_addressof(p);
				qp[1]=__builtin_addressof(q);
				auto mont{[&](char unsigned n) noexcept -> void
				{
					int unsigned chosen{static_cast<int unsigned>((k>>n)&1)};
					montgomery_curve_mont(*pp[chosen],*qp[chosen],x);
				}};
				for(;++i<8;mont(7))
					k<<=1;
				for(;j--;)
				{
					k=static_cast<char unsigned>(secret_key[j]);
					for(char unsigned m{8};m--;mont(m));
				}
				field_number_inverse(q.z.content,p.z.content);
				field_multiplication_mod(x.content,p.x.content,q.z.content);
#if __cpp_lib_bit_cast >= 201806L && __cpp_lib_is_constant_evaluated >= 201811L
				if(__builtin_is_constant_evaluated())
				{
					auto xarray{__builtin_bit_cast(::fast_io::freestanding::array<std::byte,total_len>,x)};
					::fast_io::freestanding::nonoverlapped_bytes_copy_n(xarray.data(),total_len,public_key);
				}
				else
#endif
				{
					::fast_io::freestanding::nonoverlapped_bytes_copy_n(reinterpret_cast<std::byte const*>(x.content),total_len,public_key);
				}
				return;
			}
			k<<=1;
		}
	}
	none_secure_clear(public_key,total_len);
}

inline constexpr void x25519_trim_secret_key(std::byte* x) noexcept
{
	*x = std::byte{static_cast<char8_t>(0xf8&static_cast<char8_t>(*x))};
	x[31] = std::byte{static_cast<char8_t>((static_cast<char8_t>(x[31]) | 0x40) & 0x7f)};
}

}

namespace fast_io::diffie_hellman::x25519
{

inline constexpr std::size_t key_size{32};

inline constexpr std::byte base_point[32]{std::byte{9}};

inline constexpr void calculate_public_key_to_ptr(std::byte* pk, std::byte* sk) noexcept
{
	::fast_io::curve25519::x25519_trim_secret_key(sk);
	::fast_io::curve25519::montgomery_curve_point_multiplication(pk,base_point,sk);
}

inline constexpr void create_shared_key_to_ptr(std::byte* shared,std::byte const* pk, std::byte* sk) noexcept
{
	::fast_io::curve25519::x25519_trim_secret_key(sk);
	::fast_io::curve25519::montgomery_curve_point_multiplication(shared,pk,sk);
}

#if __cpp_lib_span >= 202002L && (defined(_GLIBCXX_SPAN) || defined(_LIBCPP_SPAN) || defined(_SPAN_))
inline constexpr void calculate_public_key(std::span<std::byte,32> public_key, std::span<std::byte,32> secret_key) noexcept
{
	::fast_io::diffie_hellman::x25519::calculate_public_key_to_ptr(public_key.data(),secret_key.data());
}
inline constexpr void create_shared_key(std::span<std::byte,32> shared_key, std::span<std::byte const,32> public_key, std::span<std::byte,32> secret_key) noexcept
{
	::fast_io::diffie_hellman::x25519::create_shared_key_to_ptr(shared_key.data(),public_key.data(),secret_key.data());
}
#endif

}
