#include"pch.hpp"
#include"field.h"

int main()
{
	constexpr fast_io::curve25519::field_number a{21412,42,463,142};
#if 0
	auto inv{inverse(a)};
	auto unit{inv*a};
	auto aself{unit*a};
	println("a=",a," inv=",inv," unit=",unit," aself=",aself);
#endif
	constexpr auto inv{inverse(a)};
	constexpr auto unit{inv*a};
	println("a=",a," inv=",inv," unit=",unit);
}
