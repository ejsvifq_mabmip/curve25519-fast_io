#pragma once
#if defined(_MSC_VER)
#include <intrin.h>
#elif defined(__x86_64__) || defined(__i386__)
//#include<immintrin.h>
#endif
#include<type_traits>
namespace fast_io::intrinsics
{

template<typename T>
#if __cpp_lib_concepts >= 202002L
requires (std::unsigned_integral<T>)
#endif
inline constexpr bool add_carry_naive(bool carry,T a,T b,T& out) noexcept
{
	T temp{carry+a};
	out=temp+b;
	return (out < b) | (temp < a);
}

template<typename T>
#if __cpp_lib_concepts >= 202002L
requires (std::unsigned_integral<T>)
#endif
inline constexpr bool add_carry(bool carry,T a,T b,T& out) noexcept
{
#if __cpp_if_consteval >= 202106L
	if consteval
	{
		return add_carry_naive(carry,a,b,out);
	}
	else
#elif __cpp_lib_is_constant_evaluated >= 201811L
	if(std::is_constant_evaluated())
		return add_carry_naive(carry,a,b,out);
	else
#endif
	{
#if defined(_MSC_VER) && !defined(__clang__)
#if (defined(_M_IX86) || defined(_M_AMD64))
	if constexpr(sizeof(T)==8)
	{
#if defined(_M_AMD64)
		return _addcarryx_u64(carry,a,b,reinterpret_cast<std::uint64_t*>(__builtin_addressof(out)));
#else
		return _addcarryx_u32(_addcarryx_u32(carry,
		*reinterpret_cast<std::uint32_t*>(__builtin_addressof(a)),*reinterpret_cast<std::uint32_t*>(__builtin_addressof(b)),reinterpret_cast<std::uint32_t*>(__builtin_addressof(out))),
		reinterpret_cast<std::uint32_t*>(__builtin_addressof(a))[1],reinterpret_cast<std::uint32_t*>(__builtin_addressof(b))[1],reinterpret_cast<std::uint32_t*>(__builtin_addressof(out))+1);
#endif
	}
	else if constexpr(sizeof(T)==4)
		return _addcarryx_u32(carry,a,b,reinterpret_cast<std::uint32_t*>(__builtin_addressof(out)));
	else if constexpr(sizeof(T)==2)
		return _addcarry_u16(carry,a,b,reinterpret_cast<short unsigned*>(__builtin_addressof(out)));
	else if constexpr(sizeof(T)==1)
		return _addcarry_u8(carry,a,b,reinterpret_cast<char unsigned*>(__builtin_addressof(out)));
	else
		return add_carry_naive(carry,a,b,out);
#else
		return add_carry_naive(carry,a,b,out);
#endif
#elif defined(__has_builtin) && (__has_builtin(__builtin_addcb)&&__has_builtin(__builtin_addcs)&&__has_builtin(__builtin_addc)&&__has_builtin(__builtin_addcl)&&__has_builtin(__builtin_addcll))
	if constexpr(sizeof(T)==sizeof(long long unsigned))
	{
		long long unsigned carryout;
		out=__builtin_addcll(a,b,carry,__builtin_addressof(carryout));
		return carryout;
	}
	else if constexpr(sizeof(T)==sizeof(long unsigned))
	{
		long unsigned carryout;
		out=__builtin_addcl(a,b,carry,__builtin_addressof(carryout));
		return carryout;
	}
	else if constexpr(sizeof(T)==sizeof(unsigned))
	{
		unsigned carryout;
		out=__builtin_addc(a,b,carry,__builtin_addressof(carryout));
		return carryout;
	}
	else if constexpr(sizeof(T)==sizeof(short unsigned))
	{
		short unsigned carryout;
		out=__builtin_addcs(a,b,carry,__builtin_addressof(carryout));
		return carryout;
	}
	else if constexpr(sizeof(T)==sizeof(char unsigned))
	{
		char unsigned carryout;
		out=__builtin_addcb(a,b,carry,__builtin_addressof(carryout));
		return carryout;
	}
	else
	{
		return add_carry_naive(carry,a,b,out);
	}
#elif defined(__has_builtin) && (__has_builtin(__builtin_ia32_addcarryx_u32)||__has_builtin(__builtin_ia32_addcarry_u32)||__has_builtin(__builtin_ia32_addcarryx_u64))
	if constexpr(sizeof(T)==8)
	{
#if __has_builtin(__builtin_ia32_addcarryx_u64)
		using may_alias_ptr_type [[gnu::may_alias]] = unsigned long long*;
		return __builtin_ia32_addcarryx_u64(carry,a,b,reinterpret_cast<may_alias_ptr_type>(__builtin_addressof(out)));
#else
		std::uint32_t a_low;
		std::uint32_t a_high;
		__builtin_memcpy(__builtin_addressof(a_low),__builtin_addressof(a),4);
		__builtin_memcpy(__builtin_addressof(a_high),reinterpret_cast<char const*>(__builtin_addressof(a))+4,4);
		std::uint32_t b_low;
		std::uint32_t b_high;
		__builtin_memcpy(__builtin_addressof(b_low),__builtin_addressof(b),4);
		__builtin_memcpy(__builtin_addressof(b_high),reinterpret_cast<char const*>(__builtin_addressof(b))+4,4);
		using may_alias_ptr_type [[gnu::may_alias]] = unsigned*;
	#if __has_builtin(__builtin_ia32_addcarry_u32)
		return __builtin_ia32_addcarry_u32(__builtin_ia32_addcarry_u32(carry,a_low,b_low,reinterpret_cast<may_alias_ptr_type>(__builtin_addressof(out))),
		a_high,b_high,reinterpret_cast<may_alias_ptr_type>(__builtin_addressof(out))+1);
	#elif __has_builtin(__builtin_ia32_addcarryx_u32)
		return __builtin_ia32_addcarryx_u32(__builtin_ia32_addcarryx_u32(carry,a_low,b_low,reinterpret_cast<may_alias_ptr_type>(__builtin_addressof(out))),
		a_high,b_high,reinterpret_cast<may_alias_ptr_type>(__builtin_addressof(out))+1);
	#else
		return add_carry_naive(carry,a,b,out);
	#endif
#endif
	}
	else if constexpr(sizeof(T)==4)
	{
		using may_alias_ptr_type [[gnu::may_alias]] = unsigned*;
#if __has_builtin(__builtin_ia32_addcarry_u32)
		return __builtin_ia32_addcarry_u32(carry,a,b,reinterpret_cast<may_alias_ptr_type>(__builtin_addressof(out)));
#elif __has_builtin(__builtin_ia32_addcarryx_u32)
		return __builtin_ia32_addcarryx_u32(carry,a,b,reinterpret_cast<may_alias_ptr_type>(__builtin_addressof(out)));
#else
		return add_carry_naive(carry,a,b,out);
#endif
	}
	else
		return add_carry_naive(carry,a,b,out);	//16 bit addcarry simply does not exist on gcc and clang
#else
	return add_carry_naive(carry,a,b,out);
#endif
	}
}

template<typename T>
#if __cpp_lib_concepts >= 202002L
requires (std::unsigned_integral<T>)
#endif
inline constexpr bool sub_borrow_naive(bool carry,T a,T b,T& out) noexcept
{
	T temp{a-carry};
	out=temp-b;
	return (temp<out)|(a<temp);
}

template<typename T>
#if __cpp_lib_concepts >= 202002L
requires (std::unsigned_integral<T>)
#endif
inline constexpr bool sub_borrow(bool borrow,T a,T b,T& out) noexcept
{
#if __cpp_if_consteval >= 202106L
	if consteval
	{
		return sub_borrow_naive(borrow,a,b,out);
	}
#elif __cpp_lib_is_constant_evaluated >= 201811L
	if(std::is_constant_evaluated())
		return sub_borrow_naive(borrow,a,b,out);
	else
#endif
	{
#if defined(_MSC_VER) && !defined(__clang__)
#if (defined(_M_IX86) || defined(_M_AMD64))
	if constexpr(sizeof(T)==8)
	{
#if defined(_M_AMD64)
		return _subborrow_u64(borrow,a,b,reinterpret_cast<std::uint64_t*>(__builtin_addressof(out)));
#else
		return _subborrow_u32(_subborrow_u32(borrow,
		*reinterpret_cast<std::uint32_t*>(__builtin_addressof(a)),*reinterpret_cast<std::uint32_t*>(__builtin_addressof(b)),reinterpret_cast<std::uint32_t*>(__builtin_addressof(out))),
		reinterpret_cast<std::uint32_t*>(__builtin_addressof(a))[1],reinterpret_cast<std::uint32_t*>(__builtin_addressof(b))[1],reinterpret_cast<std::uint32_t*>(__builtin_addressof(out))+1);
#endif
	}
	else if constexpr(sizeof(T)==4)
		return _subborrow_u32(borrow,a,b,reinterpret_cast<std::uint32_t*>(__builtin_addressof(out)));
	else if constexpr(sizeof(T)==2)
		return _subborrow_u16(borrow,a,b,reinterpret_cast<short unsigned*>(__builtin_addressof(out)));
	else if constexpr(sizeof(T)==1)
		return _subborrow_u8(borrow,a,b,reinterpret_cast<char unsigned*>(__builtin_addressof(out)));
	else
		return sub_borrow_naive(borrow,a,b,out);
#else
		return sub_borrow_naive(borrow,a,b,out);
#endif
#elif defined(__has_builtin) && (__has_builtin(__builtin_subcb)&&__has_builtin(__builtin_subcs)&&__has_builtin(__builtin_subc)&&__has_builtin(__builtin_subcl)&&__has_builtin(__builtin_subcll))
	if constexpr(sizeof(T)==sizeof(long long unsigned))
	{
		long long unsigned borrowout;
		out=__builtin_subcll(a,b,borrow,__builtin_addressof(borrowout));
		return borrowout;
	}
	else if constexpr(sizeof(T)==sizeof(long unsigned))
	{
		long unsigned borrowout;
		out=__builtin_subcl(a,b,borrow,__builtin_addressof(borrowout));
		return borrowout;
	}
	else if constexpr(sizeof(T)==sizeof(unsigned))
	{
		unsigned borrowout;
		out=__builtin_subc(a,b,borrow,__builtin_addressof(borrowout));
		return borrowout;
	}
	else if constexpr(sizeof(T)==sizeof(short unsigned))
	{
		short unsigned borrowout;
		out=__builtin_subcs(a,b,borrow,__builtin_addressof(borrowout));
		return borrowout;
	}
	else if constexpr(sizeof(T)==sizeof(char unsigned))
	{
		char unsigned borrowout;
		out=__builtin_subcb(a,b,borrow,__builtin_addressof(borrowout));
		return borrowout;
	}
	else
	{
		return sub_borrow_naive(borrow,a,b,out);
	}
#elif defined(__has_builtin) && (__has_builtin(__builtin_ia32_sbb_u32)||__has_builtin(__builtin_ia32_sbb_u64) || __has_builtin(__builtin_ia32_subborrow_u64) || __has_builtin(__builtin_ia32_subborrow_u32))
	if constexpr(sizeof(T)==8)
	{
#if __has_builtin(__builtin_ia32_sbb_u64)
		using may_alias_ptr_type [[gnu::may_alias]] = unsigned long long*;
		return __builtin_ia32_sbb_u64(borrow,a,b,reinterpret_cast<may_alias_ptr_type>(__builtin_addressof(out)));
#elif __has_builtin(__builtin_ia32_subborrow_u64)
		using may_alias_ptr_type [[gnu::may_alias]] = unsigned long long*;
		return __builtin_ia32_subborrow_u64(borrow,a,b,reinterpret_cast<may_alias_ptr_type>(__builtin_addressof(out)));
#elif __has_builtin(__builtin_ia32_sbb_u32) || __has_builtin(__builtin_ia32_subborrow_u32)
		std::uint32_t a_low;
		std::uint32_t a_high;
		__builtin_memcpy(__builtin_addressof(a_low),__builtin_addressof(a),4);
		__builtin_memcpy(__builtin_addressof(a_high),reinterpret_cast<char const*>(__builtin_addressof(a))+4,4);
		std::uint32_t b_low;
		std::uint32_t b_high;
		__builtin_memcpy(__builtin_addressof(b_low),__builtin_addressof(b),4);
		__builtin_memcpy(__builtin_addressof(b_high),reinterpret_cast<char const*>(__builtin_addressof(b))+4,4);
		using may_alias_ptr_type [[gnu::may_alias]] = unsigned*;
#if __has_builtin(__builtin_ia32_sbb_u32)
		return __builtin_ia32_sbb_u32(__builtin_ia32_sbb_u32(borrow,a_low,b_low,reinterpret_cast<may_alias_ptr_type>(__builtin_addressof(out))),
		a_high,b_high,reinterpret_cast<may_alias_ptr_type>(__builtin_addressof(out))+1);
#else
		return __builtin_ia32_subborrow_u32(__builtin_ia32_subborrow_u32(borrow,a_low,b_low,reinterpret_cast<may_alias_ptr_type>(__builtin_addressof(out))),
		a_high,b_high,reinterpret_cast<may_alias_ptr_type>(__builtin_addressof(out))+1);
#endif
#else
		return sub_borrow_naive(borrow,a,b,out);
#endif
	}
	else if constexpr(sizeof(T)==4)
	{
#if __has_builtin(__builtin_ia32_sbb_u32)
		using may_alias_ptr_type [[gnu::may_alias]] = unsigned*;
		return __builtin_ia32_sbb_u32(borrow,a,b,reinterpret_cast<may_alias_ptr_type>(__builtin_addressof(out)));
#elif __has_builtin(__builtin_ia32_subborrow_u32)
		using may_alias_ptr_type [[gnu::may_alias]] = unsigned*;
		return __builtin_ia32_subborrow_u32(borrow,a,b,reinterpret_cast<may_alias_ptr_type>(__builtin_addressof(out)));
#else
		return sub_borrow_naive(borrow,a,b,out);
#endif
	}
	else
		return sub_borrow_naive(borrow,a,b,out);	//16 bit subborrow simply does not exist on gcc and clang
#else
	return sub_borrow_naive(borrow,a,b,out);
#endif
	}
}

struct ul32x2_little_endian
{
	std::uint_least32_t low,high;
};
struct ul32x2_big_endian
{
	std::uint_least32_t high,low;
};

using ul32x2 = std::conditional_t<std::endian::native==std::endian::big,ul32x2_big_endian,ul32x2_little_endian>;

inline constexpr std::uint_least32_t umul_least_32(std::uint_least32_t a,std::uint_least32_t b,std::uint_least32_t& high) noexcept
{
	if constexpr(std::endian::native==std::endian::little||std::endian::native==std::endian::big)
	{
		auto ret{__builtin_bit_cast(ul32x2,static_cast<std::uint_least64_t>(a)*b)};
		high=ret.high;
		return ret.low;
	}
	else
	{
		std::uint_least64_t v{static_cast<std::uint_least64_t>(a)*b};
		high=static_cast<std::uint_least32_t>(v>>32u);
		return static_cast<std::uint_least32_t>(v);
	}
}


inline
#if __cpp_if_consteval >= 202106L || __cpp_lib_is_constant_evaluated >= 201811L
constexpr
#endif
std::uint64_t umul_naive(std::uint64_t a,std::uint64_t b,std::uint64_t& high) noexcept
{
	std::uint32_t a0(static_cast<std::uint32_t>(a));
	std::uint32_t a1(static_cast<std::uint32_t>(a>>32));
	std::uint32_t b0(static_cast<std::uint32_t>(b));
	std::uint32_t b1(static_cast<std::uint32_t>(b>>32));
	std::uint64_t c00(static_cast<std::uint64_t>(a0)*b0);
	std::uint64_t c01(static_cast<std::uint64_t>(a0)*b1);
	std::uint64_t c10(static_cast<std::uint64_t>(a1)*b0);
	std::uint64_t c11(static_cast<std::uint64_t>(a1)*b1);

	std::uint64_t d0{static_cast<std::uint32_t>(c00)};
	std::uint64_t c00_high{c00>>32};
	std::uint64_t c01_low{static_cast<std::uint32_t>(c01)};
	std::uint64_t c01_high{c01>>32};
	std::uint64_t c10_low{static_cast<std::uint32_t>(c10)};
	std::uint64_t c10_high{c10>>32};

	std::uint64_t d1{c00_high+c01_low+c10_low};
	std::uint64_t d2{(d1>>32)+c10_high+c01_high};
	std::uint64_t d3{(d2>>32)+c11};
	high=d2|(d3<<32);
	return d0|(d1<<32);
}


inline constexpr ul32x2_little_endian unpack_ul64(std::uint_least64_t a) noexcept
{
#if defined(__has_builtin) && defined(__GNUC__) && !defined(__clang__)
#if __has_builtin(__builtin_bit_cast)
	if constexpr(std::endian::native==std::endian::little)
	{
		return __builtin_bit_cast(ul32x2_little_endian,a);	//get around gcc bug
	}
	else if constexpr(std::endian::native==std::endian::big)
	{
		auto [a1,a0]=__builtin_bit_cast(ul32x2_big_endian,a);
		return {a0,a1};
	}
	else
#endif
#endif
	{
		return {static_cast<std::uint_least32_t>(a),static_cast<std::uint_least32_t>(a>>32u)};
	}
}

inline constexpr std::uint_least64_t pack_ul64(std::uint_least32_t low,std::uint_least32_t high) noexcept
{
#if defined(__has_builtin) && defined(__GNUC__) && !defined(__clang__)
#if __has_builtin(__builtin_bit_cast)
	if constexpr(std::endian::native==std::endian::little)
	{
		return __builtin_bit_cast(std::uint_least64_t,ul32x2_little_endian{low,high});	//get around gcc bug
	}
	else if constexpr(std::endian::native==std::endian::big)
	{
		return __builtin_bit_cast(std::uint_least64_t,ul32x2_big_endian{high,low});	//get around gcc bug
	}
	else
#endif
#endif
	{
		return (static_cast<std::uint_least64_t>(high)<<32u)|low;
	}
}

inline constexpr std::uint_least64_t umul_least_64(std::uint_least64_t a,std::uint_least64_t b,std::uint_least64_t& high) noexcept
{
	auto [a0,a1]=unpack_ul64(a);
	auto [b0,b1]=unpack_ul64(b);
	std::uint_least32_t c1;
	std::uint_least32_t c0{umul_least_32(a0,b0,c1)};
	std::uint_least32_t a0b1h;
	std::uint_least32_t a0b1l{umul_least_32(a0,b1,a0b1h)};
	std::uint_least32_t a1b0h;
	std::uint_least32_t a1b0l{umul_least_32(a1,b0,a1b0h)};
	std::uint_least32_t c3;
	std::uint_least32_t c2{umul_least_32(a1,b1,c3)};
	bool carry{add_carry(false,c1,a0b1l,c1)};
	carry=add_carry(carry,a0b1h,c2,c2);
	std::uint_least32_t temp{carry};
	carry=add_carry(false,c1,a1b0l,c1);
	carry=add_carry(carry,a1b0h,c2,c2);
	add_carry(carry,temp,c3,c3);
	high=pack_ul64(c2,c3);
	return pack_ul64(c0,c1);
}

inline
#if __cpp_if_consteval >= 202106L || __cpp_lib_is_constant_evaluated >= 201811L
constexpr
#endif
std::uint64_t umul(std::uint64_t a,std::uint64_t b,std::uint64_t& high) noexcept
{
#ifdef __SIZEOF_INT128__
#if __cpp_if_consteval >= 202106L
	if consteval
	{
		__uint128_t res{static_cast<__uint128_t>(a)*b};
		high=static_cast<std::uint64_t>(res>>64u);
		return static_cast<std::uint64_t>(res);
	}
	else
#elif __cpp_lib_is_constant_evaluated >= 201811L
	if(std::is_constant_evaluated())
	{
		__uint128_t res{static_cast<__uint128_t>(a)*b};
		high=static_cast<std::uint64_t>(res>>64u);
		return static_cast<std::uint64_t>(res);
	}
	else

#endif
	{
#if defined(__has_builtin)
		if constexpr(std::endian::native==std::endian::little||std::endian::native==std::endian::big)
		{
			struct u64x2_little_endian_t
			{
				std::uint64_t low,high;
			};
			struct u64x2_big_endian_t
			{
				std::uint64_t high,low;
			};
			using u64x2_t = std::conditional_t<std::endian::native==std::endian::little,u64x2_little_endian_t,u64x2_big_endian_t>;
			static_assert(sizeof(__uint128_t)==sizeof(u64x2_t));
#if __has_builtin(__builtin_bit_cast)
			auto u{__builtin_bit_cast(u64x2_t,static_cast<__uint128_t>(a)*b)};
			high=u.high;
			return u.low;
#else
			__uint128_t res{static_cast<__uint128_t>(a)*b};
			u64x2_t u;
#if __has_builtin(__builtin_memcpy)
			__builtin_memcpy(__builtin_addressof(u),__builtin_addressof(res),sizeof(u64x2_t));
#else
			std::memcpy(__builtin_addressof(u),__builtin_addressof(res),sizeof(u64x2_t));
#endif
			high=u.high;
			return u.low;
#endif
		}
		else
#endif
		{
			__uint128_t res{static_cast<__uint128_t>(a)*b};
			high=static_cast<std::uint64_t>(res>>64u);
			return static_cast<std::uint64_t>(res);
		}
	}
#elif defined(_MSC_VER) && defined(_M_X64)
#if __cpp_if_consteval >= 202106L
	if consteval
	{
		return umul_naive(a,b,high);
	}
	else
#elif __cpp_lib_is_constant_evaluated >= 201811L
	if(std::is_constant_evaluated())
	{
		return umul_naive(a,b,high);
	}
	else
#endif
	{
		return _umul128(a,b,__builtin_addressof(high));
	}
#else
#if defined(__clang__) || (defined(__GNUC__) && defined(__i386__))
	return umul_least_64(a,b,high);
#else
	return umul_naive(a,b,high);
#endif
#endif
}

}