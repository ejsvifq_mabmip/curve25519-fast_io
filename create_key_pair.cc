//#include<fast_io.h>
#include<fast_io_crypto.h>

#include"field_number.h"
#include"custom_blindings.h"
#include"field.h"


#include"create_key_pair.h"

void ed25519_create_key_pair_test(char unsigned* public_key,char unsigned* private_key,char unsigned const* secret_key) noexcept
{
	::fast_io::curve25519::ed25519_create_key_pair(public_key,private_key,secret_key);
}