#include<cstdint>
#include"base_folding.h"
#include<fast_io.h>
#include<fast_io_device.h>

inline constexpr void output_one(fast_io::u8obuf_file& obf,std::array<std::uint64_t,4> const& a)
{
	for(std::size_t i{};i!=a.size();++i)
	{
		if(i)
			print(obf,u8",");
		print(obf,a[i],u8"ULL");
	}
}


int main()
{
	fast_io::u8obuf_file obf(u8"w2.h");
	print(obf,u8"inline constexpr field_number w2d{");
	output_one(obf,_w_2d);
	print(obf,u8"};\n"
	u8"inline constexpr field_number wdi{");
	output_one(obf,_w_di);
	print(obf,u8"};\n");
}
