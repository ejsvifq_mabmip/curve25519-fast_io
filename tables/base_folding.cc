#include<cstdint>
#include"base_folding.h"
#include<fast_io.h>
#include<fast_io_device.h>

inline constexpr void output_one(fast_io::u8obuf_file& obf,std::array<std::uint64_t,4> const& a)
{
	print(obf,u8"{");
	for(std::size_t i{};i!=a.size();++i)
	{
		if(i)
			print(obf,u8",");
		print(obf,a[i],u8"ULL");
	}
}


int main()
{
	fast_io::u8obuf_file obf(u8"folding.h");
	for(auto const& e : _w_base_folding8)
	{
		print(obf,u8"{");
		output_one(obf,e.ypx);
		print(obf,u8"},");
		output_one(obf,e.ymx);
		print(obf,u8"},");
		output_one(obf,e.t2d);
		print(obf,u8"}},\n");
	}
}