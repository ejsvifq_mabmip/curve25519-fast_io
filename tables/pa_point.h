struct affine_point
{
	field_element x,y;
};

struct precomputed_affine_point
{
	field_element y_plus_x;
	field_element y_minus_x;
	field_element t_mul_2d;
};

struct extended_point
{
	field_element x,y,z,t;
};

struct precomputed_extended_point
{
	field_element y_plus_x;
	field_element y_minus_x;
	field_element t_mul_2d;
	field_element z_mul_2;
};
