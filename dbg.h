#pragma once
namespace fast_io::curve25519
{

template<std::integral char_type>
inline constexpr std::size_t print_reserve_size(io_reserve_type_t<char_type,field_number>) noexcept
{
	using value_type = typename field_number::value_type;
	constexpr std::size_t count{32/sizeof(value_type)};
	constexpr std::size_t sz{print_reserve_size(io_reserve_type<char_type,value_type>)*count+count+1};
	return sz;
}

namespace details
{
template<std::random_access_iterator Iter>
inline constexpr Iter print_reserve_fd_nimpl(Iter iter,field_number const& f) noexcept
{
	using char_type = std::iter_value_t<Iter>;
	using value_type = typename field_number::value_type;
	if constexpr(std::same_as<char_type,char>)
		*iter='[';
	else if constexpr(std::same_as<char_type,wchar_t>)
		*iter=L'[';
	else
		*iter=u8'[';
	++iter;
	for(auto const e : f.content)
	{
		iter=print_reserve_define(io_reserve_type<char_type,value_type>,iter,e);
		if constexpr(std::same_as<char_type,char>)
			*iter=',';
		else if constexpr(std::same_as<char_type,wchar_t>)
			*iter=L',';
		else
			*iter=u8',';
		++iter;
	}
	auto itm1{iter};
	--itm1;
	if constexpr(std::same_as<char_type,char>)
		*itm1=']';
	else if constexpr(std::same_as<char_type,wchar_t>)
		*itm1=L']';
	else
		*itm1=u8']';
	return iter;
}
}

template<std::random_access_iterator Iter>
inline constexpr Iter print_reserve_define(io_reserve_type_t<std::iter_value_t<Iter>,field_number>,
	Iter iter,field_number const& f) noexcept
{
	return details::print_reserve_fd_nimpl(iter,f);
}

}