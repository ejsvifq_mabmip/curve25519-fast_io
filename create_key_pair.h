#pragma once

namespace fast_io::curve25519
{

namespace details
{
#if 0
inline constexpr void ed25519_sha512_trim_secret_key_impl(::fast_io::sha512& sha) noexcept
{
#if __cpp_lib_bit_cast >= 201806L && __cpp_lib_is_constant_evaluated >= 201811L
	if (std::is_constant_evaluated())
	{
		auto temp {__builtin_bit_cast(::fast_io::freestanding::array<char unsigned,sizeof(sha.digest_block)>,sha.digest_block)};
		x25519_trim_secret_key(temp.data());
		sha.digest_block=__builtin_bit_cast(decltype(sha.digest_block),temp);
	}
	else
#endif
	{
		x25519_trim_secret_key(reinterpret_cast<char unsigned*>(sha.digest_block.data()));
	}
}
}

#endif


template<std::endian end,std::unsigned_integral U>
inline
#if __cpp_if_consteval >= 202106L || __cpp_lib_is_constant_evaluated >= 201811L
constexpr
#endif
void hash_digest_to_ptr_common_impl(std::byte const *ptr,std::size_t n,U *digest) noexcept
{
	constexpr std::size_t usz{sizeof(U)};
#if __cpp_if_consteval >= 202106L || __cpp_lib_is_constant_evaluated >= 201811L
#if __cpp_if_consteval >= 202106L
	if consteval
#else
	if(__builtin_is_constant_evaluated())
#endif
	{
		for(std::size_t i{};i!=n;++i)
		{
			::fast_io::freestanding::array<::std::byte,usz> va;
			::fast_io::freestanding::nonoverlapped_bytes_copy_n(ptr,usz,va.data());
			U v{__builtin_bit_cast(U,va)};
			if constexpr(::std::endian::native==end)
			{
				v=::fast_io::byte_swap(v);
			}
			digest[i]=v;
			ptr+=usz;
		}
	}
	else
#endif
	{
		if constexpr(::std::endian::native==end)
		{
			::fast_io::freestanding::nonoverlapped_bytes_copy_n(ptr,n*sizeof(U),reinterpret_cast<std::byte*>(digest));
		}
		else
		{
			for(std::size_t i{};i!=n;++i)
			{
				U t;
				::fast_io::freestanding::nonoverlapped_bytes_copy_n(ptr,usz,reinterpret_cast<std::byte*>(__builtin_addressof(t)));
				t=::fast_io::byte_swap(t);
				::fast_io::freestanding::nonoverlapped_bytes_copy_n(reinterpret_cast<std::byte const*>(__builtin_addressof(t)),usz,reinterpret_cast<std::byte*>(digest[i]));
			}
		}
	}
}


#if (!defined(_MSC_VER) || defined(__clang__)) && (defined(__SSE4_2__) || defined(__wasm_simd128__))

template<std::endian end,std::unsigned_integral U>
inline constexpr void hash_digest_to_ptr_simd16_impl(std::byte const* ptr,std::size_t n,U* digest) noexcept
{
	constexpr std::size_t usz{sizeof(U)};
#if __cpp_if_consteval >= 202106L || __cpp_lib_is_constant_evaluated >= 201811L
#if __cpp_if_consteval >= 202106L
	if consteval
#else
	if(__builtin_is_constant_evaluated())
#endif
	{
		hash_digest_to_ptr_common_impl(ptr,n,digest);
	}
	else
#endif
	{
		if constexpr(::std::endian::native==end)
		{
			::fast_io::freestanding::nonoverlapped_bytes_copy_n(ptr,n*sizeof(U),reinterpret_cast<std::byte*>(digest));
		}
		else
		{
			constexpr std::size_t sixteen{16u};
			constexpr std::size_t factor{sixteen/usz};
			static_assert(sixteen%usz==0&&usz!=sixteen);
			::fast_io::intrinsics::simd_vector<U,factor> s;
			std::byte* i{reinterpret_cast<std::byte const*>(ptr)};
			std::byte* e{reinterpret_cast<std::byte const*>(ptr+n)};
			for(;i!=e;i+=sixteen)
			{
				s.load(i);
				s.swap_endian();
				s.store(digest);
				digest+=factor;
			}
		}
	}
}
#endif

template<std::unsigned_integral digest_value_type,std::size_t digest_size,std::endian end>
requires (sizeof(digest_value_type)!=0&&(digest_size%sizeof(digest_value_type)==0))
inline constexpr void hash_digest_to_byte_ptr_common(std::byte const* ptr,digest_value_type* digest) noexcept
{
	constexpr std::size_t sz{digest_size/sizeof(digest_value_type)};
	static_assert(sz!=0);
#if (!defined(_MSC_VER) || defined(__clang__)) && (defined(__SSE4_2__) || defined(__wasm_simd128__))
	if constexpr(sz%16u==0)
	{
		hash_digest_to_ptr_simd16_impl<end>(ptr,sz,digest);
	}
	else
#endif
	{
		hash_digest_to_byte_ptr_common<end>(ptr,sz,digest);
	}
}

inline
#if __cpp_lib_bit_cast >= 201806L && __cpp_lib_is_constant_evaluated >= 201811L
constexpr
#endif
void ed25519_create_key_pair(std::byte* public_key,std::byte* private_key,std::byte const* secret_key) noexcept
{
	constexpr std::size_t keylength{32};
	constexpr std::size_t keylengthm1{keylength-1};
	::fast_io::sha512_context sha;
	sha.update(secret_key,keylength);
	sha.final();
	constexpr std::size_t digest_size{::fast_io::sha512_context::digest_size};
	std::byte md[digest_size];
	sha.digest_to_byte_ptr(md);

	x25519_trim_secret_key(md);
	std::uint_least64_t t[4];
	hash_digest_to_byte_ptr_common<std::uint_least64_t,4,::std::endian::big>(md,t);

	affine_point q;
	base_point_multiply(__builtin_addressof(q),t);
#if __cpp_lib_bit_cast >= 201806L && __cpp_lib_is_constant_evaluated >= 201811L
	if (std::is_constant_evaluated())
	{
		auto temp{__builtin_bit_cast(::fast_io::freestanding::array<std::byte,keylength>,q.y.content)};
		::fast_io::freestanding::nonoverlapped_bytes_copy_n(temp.data(),temp.size(),public_key);
	}
	else
#endif
	{
		::fast_io::freestanding::nonoverlapped_bytes_copy_n(reinterpret_cast<std::byte const*>(q.y.content),keylength,public_key);
	}
	constexpr char unsigned mask{(1u<<7u)-1u};
	public_key[keylengthm1]&=mask;
	public_key[keylengthm1]|=static_cast<char unsigned>((*q.x.content&1u)<<7u);

	::fast_io::freestanding::nonoverlapped_bytes_copy_n(secret_key,keylength,private_key);
	::fast_io::freestanding::nonoverlapped_bytes_copy_n(public_key,keylength,private_key+keylength);
}

#if 0
inline constexpr void ed25519_sign_message(char unsigned* signature,char unsigned const* private_key,char unsigned const* msg,std::size_t msg_size) noexcept
{
	::fast_io::sha512 sha;
	::fast_io::hash_processor processor(sha);
	::fast_io::curve25519::details::ed25519_sha512_special_impl(processor,private_key);

	::fast_io::curve25519::details::ed25519_sha512_do_final_endian_impl(processor);

	::fast_io::curve25519::details::ed25519_sha512_trim_secret_key_impl(sha);

	field_number a{};

	auto a{__builtin_bit_cast(field_number,sha)};

//	processor.reset();
	secure_clear(__builtin_addressof(sha),sizeof(sha));
}
#endif
}
