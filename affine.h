#pragma once

namespace fast_io::curve25519
{

struct affine_point
{
	field_number x,y;
};

struct pre_computed_extended_point
{
	field_number ypx;
	field_number ymx;
	field_number t2d;
	field_number z2;
};

struct pre_computed_affine_point
{
	field_number ypx;
	field_number ymx;
	field_number t2d;
	field_number z2;
};

struct xz_point
{
	field_number x,y;
};

inline constexpr field_number ecp_base_point{9};

}