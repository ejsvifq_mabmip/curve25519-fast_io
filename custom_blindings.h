#pragma once

namespace fast_io::curve25519::custom_blindings
{

inline constexpr ::fast_io::freestanding::array<std::uint32_t,8> zr_raw{0xC3BDA9AA,0xBBDEB203,0x7615BE2F,0x7A9856FA,0xA570CB56,0xCF349BBD,0xB96CED54,0xF3514844};

inline constexpr field_number zr{__builtin_bit_cast(field_number,zr_raw)};

}
