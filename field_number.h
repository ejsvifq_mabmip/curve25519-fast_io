#pragma once

namespace fast_io::curve25519
{

struct field_number
{
	using value_type = std::uint_least64_t;
	value_type content[4];
	inline constexpr value_type const& operator[](std::size_t pos) const noexcept
	{
		return content[pos];
	}
	inline constexpr value_type& operator[](std::size_t pos) noexcept
	{
		return content[pos];
	}
	inline constexpr value_type const* data() const noexcept
	{
		return content;
	}
	inline constexpr value_type* data() noexcept
	{
		return content;
	}
};

}