#pragma once

namespace fast_io::curve25519
{
inline constexpr field_number operator+(field_number const& x,field_number const& y) noexcept
{
	using namespace ::fast_io::details::intrinsics;
	field_number f;
	field_number_addition(f.content,x.content,y.content);
	return f;
}

inline constexpr field_number& operator+=(field_number& x,field_number const& y) noexcept
{
	return x=x+y;
}

inline constexpr field_number operator-(field_number const& x,field_number const& y) noexcept
{
	field_number f;
	field_number_subtraction(f.content,x.content,y.content);
	return f;
}


inline constexpr field_number& operator-=(field_number& x,field_number const& y) noexcept
{
	return x=x-y;
}

inline constexpr bool operator<(field_number const& x,field_number const& y) noexcept
{
	using namespace ::fast_io::details::intrinsics;
	using unsigned_type = field_number::value_type;
	unsigned_type temp;
	bool borrow{sub_borrow(false,x[0],y[0],temp)};
	borrow=sub_borrow(borrow,x[1],y[1],temp);
	borrow=sub_borrow(borrow,x[2],y[2],temp);
	return sub_borrow(borrow,x[3],y[3],temp);
}

inline constexpr bool operator!=(field_number const& x,field_number const& y) noexcept
{
	return (x[0]^y[0])|(x[1]^y[1])|(x[2]^y[2])|(x[3]^y[3]);
}

inline constexpr bool operator==(field_number const& x,field_number const& y) noexcept
{
	return !(x!=y);
}

inline constexpr bool operator>(field_number const& x,field_number const& y) noexcept
{
	return y<x;
}

inline constexpr bool operator<=(field_number const& x,field_number const& y) noexcept
{
	return !(y<x);
}

inline constexpr bool operator>=(field_number const& x,field_number const& y) noexcept
{
	return !(x<y);
}
}