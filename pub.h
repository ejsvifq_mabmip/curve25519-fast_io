#pragma once

#include"base_folding.h"

namespace fast_io::curve25519
{

struct affine_point
{
	field_number x,y;
};

struct precomputed_affine_point
{
	field_number ypx;
	field_number ymx;
	field_number t2d;
};

struct extended_point
{
	field_number x,y,z,t;
};

struct precomputed_extended_point
{
	field_number ypx;
	field_number ymx;
	field_number t2d;
	field_number z2;
};

inline constexpr field_number w2d{16993941304535871833ULL,63073048630374742ULL,1841551078520508720ULL,2596001775599221991ULL};
inline constexpr field_number wdi{2729447966259148867ULL,819046656689919022ULL,3104705366353104742ULL,4652357855831510595ULL};

inline constexpr field_number wprime{0xFFFFFFFFFFFFFFED,0xFFFFFFFFFFFFFFFF,0xFFFFFFFFFFFFFFFF,0x7FFFFFFFFFFFFFFF};
inline constexpr field_number w_maxprime{0xFFFFFFFFFFFFFFDA,0xFFFFFFFFFFFFFFFF,0xFFFFFFFFFFFFFFFF,0xFFFFFFFFFFFFFFFF};


namespace edwards_curve
{

inline constexpr auto folding1{folding[1]};

inline constexpr void add_affine_point(extended_point* p,precomputed_affine_point const* q) noexcept
{
	field_number a,b,c,d,e;
	field_number_subtraction(a.content,p->y.content,p->x.content);
	field_number_multiplication(a.content,a.content,q->ymx.content);
	field_number_addition(b.content,p->y.content,p->x.content);
	field_number_multiplication(b.content,b.content,q->ypx.content);
	field_number_multiplication(c.content,p->t.content,q->t2d.content);
	field_number_addition(d.content,p->z.content,p->z.content);
	field_number_subtraction(e.content,b.content,a.content);
	field_number_addition(b.content,b.content,a.content);
	field_number_subtraction(a.content,d.content,c.content);
	field_number_addition(d.content,d.content,c.content);
	field_number_multiplication(p->x.content,e.content,a.content);
	field_number_multiplication(p->y.content,b.content,d.content);
	field_number_multiplication(p->t.content,e.content,b.content);
	field_number_multiplication(p->z.content,d.content,a.content);
}

inline constexpr void add_base_point(extended_point* p) noexcept
{
	add_affine_point(p,__builtin_addressof(folding1));
}

inline constexpr void edp_double_point(extended_point* p) noexcept
{
	field_number a,b,c,d,e;
	field_number_square(a.content,p->x.content);
	field_number_square(b.content,p->y.content);
	field_number_square(c.content,p->z.content);
	field_number_addition(c.content,c.content,c.content);
	field_number_addition(d.content,w_maxprime.content,a.content);

	field_number_subtraction(a.content,d.content,b.content);
	field_number_addition(d.content,d.content,b.content);
	field_number_subtraction(b.content,d.content,c.content);
	field_number_addition(e.content,p->x.content,p->y.content);
	field_number_square(e.content,e.content);
	field_number_addition(e.content,e.content,a.content);

	field_number_multiplication(p->x.content,e.content,b.content);
	field_number_multiplication(p->y.content,a.content,d.content);
	field_number_multiplication(p->z.content,d.content,b.content);
	field_number_multiplication(p->t.content,e.content,a.content);
}

inline constexpr std::uint_least32_t rl_msbs(std::uint_least32_t aa,std::uint_least64_t& xx) noexcept
{
	using namespace fast_io::details::intrinsics;
	bool carry{add_carry(false,xx,xx,xx)};
	add_carry(carry,aa,aa,aa);
	constexpr std::uint_least64_t mask{static_cast<std::uint_least64_t>(1)<<32};
	carry = ((xx&mask)==mask);
	return aa+aa+carry;
}

inline constexpr void ecp_8folds(std::byte* y,std::uint_least64_t const* x) noexcept
{
	std::uint_least64_t x0{x[0]},x1{x[1]},x2{x[2]},x3{x[3]};
	std::uint_least32_t low{};
	for(std::uint_least8_t i{};i!=32;++i)
	{
		low=rl_msbs(low,x3);
		low=rl_msbs(low,x2);
		low=rl_msbs(low,x1);
		low=rl_msbs(low,x0);
		y[i]=static_cast<std::byte>(low);
	}
}

inline constexpr std::uint_least64_t rl_msb(std::uint_least32_t& aa,std::uint_least64_t xx) noexcept
{
	using namespace fast_io::details::intrinsics;
	bool carry{add_carry(false,xx,xx,xx)};
	add_carry(carry,aa,aa,aa);
	return xx;
}

inline constexpr void ecp_4folds(std::byte* y,std::uint_least64_t const* x) noexcept
{
	std::uint_least64_t x0{x[0]},x1{x[1]},x2{x[2]},x3{x[3]};
	for(std::uint_least8_t i{};i!=64;++i)
	{
		std::uint_least32_t low{};
		x3=rl_msb(low,x3);
		x2=rl_msb(low,x2);
		x1=rl_msb(low,x1);
		x0=rl_msb(low,x0);
		y[i]=static_cast<std::byte>(low);
	}
}

inline constexpr void base_point_mult(extended_point* s,std::uint_least64_t const* sk,std::uint_least64_t const* r) noexcept
{
	std::byte cut[32];
	ecp_8folds(cut,sk);
	auto p0{folding+static_cast<char unsigned>(*cut)};
	field_number_subtraction(s->x.content,p0->ypx.content,p0->ymx.content);
	field_number_addition(s->y.content,p0->ypx.content,p0->ymx.content);
	field_number_multiplication(s->t.content,p0->t2d.content,wdi.content);

	field_number_addition(s->z.content,r,r);
	field_number_multiplication(s->x.content,s->x.content,r);
	field_number_multiplication(s->t.content,s->t.content,r);
	field_number_multiplication(s->y.content,s->y.content,r);

	for(std::size_t i{1};i!=32;++i)
	{
		edp_double_point(s);
		add_affine_point(s,folding+static_cast<char unsigned>(cut[i]));
	}
}

//to do, deal with blinding
inline constexpr void base_point_multiply(affine_point* r,std::uint_least64_t const* sk)
{
	extended_point s;
	base_point_mult(__builtin_addressof(s),sk,::fast_io::curve25519::custom_blindings::zr.content);
	field_number_inverse(s.z.content,s.z.content);
	field_multiplication_mod(r->x.content,s.x.content,s.z.content);
	field_multiplication_mod(r->y.content,s.y.content,s.z.content);
}

inline constexpr void edp_ext_point_2e(precomputed_extended_point* r,extended_point const* p) noexcept
{
	field_number_addition(r->ypx.content,p->y.content,p->x.content);
	field_number_subtraction(r->ymx.content,p->y.content,p->x.content);
	field_number_multiplication(r->t2d.content,p->t.content,w2d.content);
	field_number_addition(r->z2.content,p->z.content,p->z.content);
}

}

}
