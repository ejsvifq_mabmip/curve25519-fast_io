#include<fast_io_core.h>

inline constexpr std::uint64_t muladd_w0(std::uint64_t add_value,std::uint64_t mul_value,std::uint64_t& high) noexcept
{
	using namespace ::fast_io::details::intrinsics;
	constexpr std::uint64_t constant{38};
	constexpr std::uint64_t zero{};
	std::uint64_t low{umul(mul_value,constant,high)};
	bool carry{add_carry(false,add_value,low,low)};
	add_carry(carry,zero,high,high);
	return low;
}

inline constexpr std::uint64_t muladd_w1(std::uint64_t add_value,std::uint64_t mul_value,std::uint64_t& high,std::uint64_t last_high) noexcept
{
	using namespace ::fast_io::details::intrinsics;
	constexpr std::uint64_t constant{38};
	constexpr std::uint64_t zero{};
	std::uint64_t low{umul(mul_value,constant,high)};
	add_carry(add_carry(false,last_high,low,low),zero,high,high);
	add_carry(add_carry(false,add_value,low,low),zero,high,high);
	return low;
}

inline constexpr void field_number_square(std::uint64_t* __restrict r,std::uint64_t const* __restrict x) noexcept
{
	using namespace ::fast_io::details::intrinsics;
	constexpr std::uint64_t zero{};
	constexpr std::uint64_t constant{38};

	std::uint64_t x0{*x},x1{x[1]},x2{x[2]},x3{x[3]};

	std::uint64_t a2;
	std::uint64_t a1{umul(x0,x1,a2)};

	std::uint64_t b0;
	std::uint64_t a3{umul(x0,x3,b0)};

	std::uint64_t b2;
	std::uint64_t b1{umul(x2,x3,b2)};

	std::uint64_t b3_x0x2;
	std::uint64_t a0{umul(x0,x2,b3_x0x2)};

	std::uint64_t c1;
	std::uint64_t c0{umul(x1,x3,c1)};

	std::uint64_t c0_x1x2;
	std::uint64_t b3{umul(x1,x2,c0_x1x2)};

	bool carry{add_carry(false,b3_x0x2,b3,b3)};
	carry=add_carry(carry,c0,c0_x1x2,c0);
	add_carry(carry,zero,c1,c1);

	carry=add_carry(false,a0,a2,a2);
	carry=add_carry(carry,b3,a3,a3);
	carry=add_carry(carry,c0,b0,b0);
	carry=add_carry(carry,c1,b1,b1);
	add_carry(carry,zero,b2,b2);

// Multiply by 2
	carry=add_carry(false,a1,a1,a1);
	carry=add_carry(carry,a2,a2,a2);
	carry=add_carry(carry,a3,a3,a3);
	carry=add_carry(carry,b0,b0,b0);
	carry=add_carry(carry,b1,b1,b1);
	carry=add_carry(carry,b2,b2,b2);
	add_carry(carry,zero,zero,b3);

	std::uint64_t y1;
	x0=umul(x0,x0,y1);

	std::uint64_t y3;
	x1=umul(x1,x1,y3);

	std::uint64_t y5;
	x2=umul(x2,x2,y5);

	std::uint64_t y7;
	x3=umul(x3,x3,y7);

	carry=add_carry(false,a1,y1,y1);
	carry=add_carry(carry,a2,x1,x1);
	carry=add_carry(carry,a3,y3,y3);
	carry=add_carry(carry,b0,x2,x2);
	carry=add_carry(carry,b1,y5,y5);
	carry=add_carry(carry,b2,x3,x3);
	carry=add_carry(carry,b3,y7,y7);

	std::uint64_t high;
	a0=muladd_w0(x0,x2,high);
	a1=muladd_w1(y1,y5,high,high);
	a2=muladd_w1(x1,x3,high,high);
	a3=muladd_w1(y3,y7,high,high);

	std::uint64_t low{umul(constant,high,high)};

	carry=add_carry(false,low,a0,a0);
	carry=add_carry(carry,high,a1,a1);
	carry=add_carry(carry,zero,a2,a2);
	carry=add_carry(carry,zero,a3,a3);
	sub_borrow(carry,low,low,low);

	low&=constant;
	carry=add_carry(false,low,a0,*r);
	carry=add_carry(carry,zero,a1,r[1]);
	carry=add_carry(carry,zero,a2,r[2]);
	add_carry(carry,zero,a3,r[3]);
}

[[gnu::flatten]]
void calsquare_show(std::uint64_t* __restrict r,std::uint64_t const* __restrict x)
{
	field_number_square(r,x);
}
