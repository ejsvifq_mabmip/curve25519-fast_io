CXX = clang++
FASTIOINCLUDEPATH = -I../fast_io/include
CXXFLAGS = -Ofast -march=native -std=c++20 -fno-exceptions -fno-rtti -fno-unwind-tables -fno-asynchronous-unwind-tables
CXXEXTRAFLAGS = -flto
LDFLAGS = -fuse-ld=lld -s

all: compile.exe compile.s
	./compile.exe
compile.exe:compile.o
	$(CXX) -o compile compile.o $(CXXFLAGS) $(CXXEXTRAFLAGS) $(LDFLAGS)
compile.o:compile.cc field.h pch.hpp.gch
	$(CXX) -c compile.cc --include pch.hpp $(CXXFLAGS) $(CXXEXTRAFLAGS)
compile.s:compile.cc field.h pch.hpp.gch
	$(CXX) -S compile.cc --include pch.hpp $(CXXFLAGS)
pch.hpp.gch:pch.hpp
	$(CXX) -c pch.hpp $(CXXFLAGS) $(FASTIOINCLUDEPATH)
clean:
	rm compile.o compile.s
distclean:
	rm pch.hpp.gch compile.o compile.s