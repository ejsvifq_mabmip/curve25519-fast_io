void test_addcarry2(long long unsigned* a,long long unsigned* b,long long unsigned* c) noexcept
{
	long long unsigned carry;
	c[0]=__builtin_addcll(a[0], b[0], 0,&carry);
	c[1]=__builtin_addcll(a[1], b[1], carry,&carry);
	c[2]=__builtin_addcll(a[2], b[2], carry,&carry);
	c[3]=__builtin_addcll(a[3], b[3], carry,&carry);
}

void test_addcarry2(_ExtInt(256)* a,_ExtInt(256)* b,_ExtInt(256)* c) noexcept
{
	*c=*a+*b;
}

/*
https://godbolt.org/z/8sWxsW7Gq
*/