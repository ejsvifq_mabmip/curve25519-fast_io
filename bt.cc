#include<fast_io.h>
#include"pch.hpp"
#include"field.h"

inline constexpr std::uint32_t rl_msbs(std::uint32_t aa,std::uint64_t& xx) noexcept
{
	using namespace fast_io::details::intrinsics;
	bool carry{add_carry(false,xx,xx,xx)};
	add_carry(carry,aa,aa,aa);
	constexpr std::uint64_t mask{static_cast<std::uint64_t>(1)<<32};
	carry = ((xx&mask)==mask);
	return aa+aa+carry;
}

inline constexpr void ecp_8folds(std::byte* y,std::uint64_t const* x) noexcept
{
	std::uint64_t x0{x[0]},x1{x[1]},x2{x[2]},x3{x[3]};
	std::uint32_t low{};
	for(std::uint_least8_t i{};i!=32;++i)
	{
		low=rl_msbs(low,x3);
		low=rl_msbs(low,x2);
		low=rl_msbs(low,x1);
		low=rl_msbs(low,x0);
		y[i]=static_cast<std::byte>(low);
	}
}

inline constexpr std::uint64_t rl_msb(std::uint32_t& aa,std::uint64_t xx) noexcept
{
	using namespace fast_io::details::intrinsics;
	bool carry{add_carry(false,xx,xx,xx)};
	add_carry(carry,aa,aa,aa);
	return xx;
}

inline constexpr void ecp_4folds(std::byte* y,std::uint64_t const* x) noexcept
{
	std::uint64_t x0{x[0]},x1{x[1]},x2{x[2]},x3{x[3]};
	for(std::uint_least8_t i{};i!=64;++i)
	{
		std::uint32_t low{};
		x3=rl_msb(low,x3);
		x2=rl_msb(low,x2);
		x1=rl_msb(low,x1);
		x0=rl_msb(low,x0);
		y[i]=static_cast<std::byte>(low);
	}
}
